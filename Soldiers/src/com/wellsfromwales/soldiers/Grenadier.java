package com.wellsfromwales.soldiers;

public class Grenadier extends Soldier {
	
	public Grenadier() {
		super(new FireGrenade());
	}

	public static void main(String[] args) {
		Soldier grenadier = new Grenadier();
		grenadier.fire();
	}

}
