package com.wellsfromwales.soldiers;

public abstract class Soldier {
	private FireAction fireAction;
	
	public Soldier(FireAction fireAction) {
		this.fireAction = fireAction;
		this.fireAction.setCarrier(getClass());
	}
	
	public void fire() {
		this.fireAction.fire();
	}
	
	public void setFireAction( FireAction action ) {
		fireAction = action;
		fireAction.setCarrier( getClass() );
	}
}
