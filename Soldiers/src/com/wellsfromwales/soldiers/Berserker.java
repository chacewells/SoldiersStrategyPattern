package com.wellsfromwales.soldiers;

public class Berserker extends Soldier {
	
	public Berserker() {
		super(new FireGrenade());
	}

	public static void main(String[] args) {
		Berserker b = new Berserker();
		b.fire();
		b.setFireAction( new FireShotgun() );
		b.fire();
	}

}
