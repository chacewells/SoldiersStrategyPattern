package com.wellsfromwales.soldiers;

public interface FireAction {
	public void fire();
	public void setCarrier(Class<? extends Soldier> caller);
}
