package com.wellsfromwales.soldiers;

public class FireShotgun implements FireAction {
	private Class<? extends Soldier> carrier;

	@Override
	public void fire() {
		String[] fullNameArray = carrier.getName().split("\\.");
		String base = fullNameArray[ fullNameArray.length - 1 ];
		System.out.printf("%s says: buckshot deployed!%n", base);
	}

	@Override
	public void setCarrier(Class<? extends Soldier> caller) {
		carrier = caller;
	}

}
