package com.wellsfromwales.soldiers;

public class FireGrenade implements FireAction {
	private Class<? extends Soldier> carrier;

	@Override
	public void fire() {
		String[] fullNameArray = carrier.getName().split("\\.");
		String baseName = fullNameArray[ fullNameArray.length - 1 ];
		System.out.printf("%s says: Grenades away! Fire in the hole!%n",
				baseName);
	}

	@Override
	public void setCarrier(Class<? extends Soldier> caller) {
		carrier = caller;
	}

}
